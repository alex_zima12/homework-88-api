const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./config");
const app = express();
const port = 8000;
const posts = require('./app/posts');
const users = require('./app/users');
const comments = require('./app/comments');

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
    await mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true, autoIndex: true, useUnifiedTopology: true });

    app.use("/posts", posts);
    app.use("/comments", comments);
    app.use("/users", users);

    console.log("Connected to mongo DB");

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);