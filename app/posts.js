const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const auth = require('../middleware/auth');
const Post = require("../models/Post");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    try {
        const posts = await Post.find().sort({datetime: -1}) .populate("user");
        res.send(posts);
    } catch (e) {
        res.status(500).send(e);
    }
});
router.get("/:id", async (req, res) => {
    const result = await Post.findById(req.params.id).populate("user");
    if (result) {
        res.send(result);
    } else {
        res.sendStatus(404);
    }
});
router.post("/", auth, upload.single("image"), async (req, res) => {
    req.body.user = req.user._id;
    const postData = req.body;
    if (req.file) {
        postData.image = req.file.filename;
    }
    const post = new Post(postData);
    try {
        await post.save();
        res.send(post);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;