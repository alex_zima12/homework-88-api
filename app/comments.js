const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const auth = require('../middleware/auth');
const Comment = require("../models/Comment");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    let comment;
    try {
        if (req.query.post) {
            comment = await Comment.find({"post": req.query.post}).populate("user");
        } else {
            comment = await Comment.find();
        }
        res.send(comment);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", auth, upload.single("image"),async (req, res) => {
    req.body.user = req.user._id;
    const commentData = req.body;
    if (req.file) {
        commentData.image = req.file.filename;
    }
    const comment = new Comment(commentData);
    try {
        await comment.save();
        res.send(comment);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;