const mongoose = require("mongoose");
const idValidator = require("mongoose-id-validator");

const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        require: true
    },
    description: {
        type: String,
        required: true
    },
    image: String,
    post: {
        type: Schema.Types.ObjectId,
        ref: "Post",
        require: true
    },
});

CommentSchema.plugin(idValidator, {
    message: 'Bad ID value for {PATH}'
});

const Comment = mongoose.model("Comment", CommentSchema);
module.exports = Comment;