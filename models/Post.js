const mongoose = require("mongoose");
const idValidator = require("mongoose-id-validator");

const Schema = mongoose.Schema;

const PostSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    image: String,
    datetime: {
        type : Date,
        default: Date.now
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        require: true
    }
});

PostSchema.plugin(idValidator, {
    message: 'Bad ID value for {PATH}'
});

const Post = mongoose.model("Post", PostSchema);
module.exports = Post;